//===-- LCC.h -*- C++ -*-===//
//
//
//===----------------------------------------------------------------------===//
#ifndef LCC_H
#define LCC_H

#include <string>

namespace LCC {

constexpr char magicByte = 'o';
constexpr char headBodySeperator = 'C';
constexpr char assemblyExtension[] = "a";
constexpr char objectExtension[] = "e";
constexpr char executableExtension[] = "e";
constexpr uint16_t bits0_10 = 0b11111111111;
constexpr uint16_t bits0_8 = 0b111111111;

} // namespace LCC

#endif /* LCC_H */
