//===-- LCCMachineInfo.h -*- C++ -*-===//
#ifndef UTILITIES_LCCMACHINEINFO_H
#define UTILITIES_LCCMACHINEINFO_H

namespace LCC {

namespace RegisterInfo {
enum { R0 = 0, R1 = 1, R2 = 2, R3 = 3, R4 = 4, R5 = 5, R6 = 6, R7 = 7 };
enum { FP = R5, SP = R6, LR = R7 };
} // namespace RegisterInfo

using namespace RegisterInfo;

} // namespace LCC

#endif /* UTILITIES_LCCMACHINEINFO_H */
