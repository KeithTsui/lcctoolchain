//===-- utilities.h -*- C++ -*-===//
#ifndef UTILITIES_H
#define UTILITIES_H

#include "llvm/ADT/StringRef.h"
#include <_types/_uint16_t.h>
#include <_types/_uint8_t.h>
#include <array>
#include <cassert>
#include <cmath>
#include <cstddef>
#include <cstdint>
#include <iostream>
#include <limits>
#include <sys/_types/_int16_t.h>
#include <vector>

namespace LCC {
template <typename T, typename U> T interpretAs(U v) {
  return *reinterpret_cast<T *>(&v);
}
uint16_t toWord(char c);
uint16_t nextWord(const unsigned char *&ptr);
uint16_t getWord(const unsigned char *ptr, size_t offset);
void setWord(unsigned char *ptr, size_t offset, uint16_t word);
uint8_t nextByte(const unsigned char *&ptr);
std::vector<uint16_t> getWString(const unsigned char *&ptr);
std::vector<uint8_t> getString(const unsigned char *&ptr);
std::string toString(std::vector<uint16_t> const &wstr);
std::string toString2(std::vector<uint8_t> const &wstr);

unsigned char highChar(uint16_t word);
unsigned char lowChar(uint16_t word);
bool signOf(uint16_t a, size_t pos);
uint16_t negate(uint16_t a);
uint16_t signComplement(uint16_t a, size_t pos);
uint16_t signedExtension(uint16_t a, size_t p);
// p, q specify the sign bit position, or the size of a, b
// which are less than or equal to 16.
// e.g pc += offset9, sum(pc, 16, offset9, 8)
uint16_t signedSum(uint16_t a, size_t p, uint16_t b, size_t q,
                   bool isSubtraction);

int16_t Mult(int16_t a, uint16_t b);
uint16_t signedMul(uint16_t a, size_t p, uint16_t b, size_t q);
int16_t Divi(int16_t a, int16_t b);
uint16_t signedDiv(uint16_t a, size_t p, uint16_t b, size_t q);
void signedSumInplace(uint16_t &a, size_t p, uint16_t b, size_t q,
                      bool isSubtraction);

template <typename T> uint16_t convertToUInt16(T a, size_t bits) {
  assert(bits <= 16 && "bits must less than or equal to 16 for uint16_t.");

  auto half = std::pow(2, bits - 1);
  auto min = -half;
  auto max = half - 1;

  assert((min <= a && a <= max) &&
         "Converting argument should fit in given bits");

  int16_t b = static_cast<int16_t>(a);
  uint16_t c = *reinterpret_cast<uint16_t *>(&b);
  uint16_t extractor = std::pow(2, bits) - 1;
  uint16_t ret = c & extractor;
  return ret;
}

int16_t convertBacktoInt(uint16_t a, size_t bits);

std::array<uint8_t, 2> toUInt8s(uint16_t word);
std::vector<uint8_t> toUInt8s(llvm::StringRef str);

} // namespace LCC

#endif /* UTILITIES_H */
