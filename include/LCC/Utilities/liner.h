//===-- Liner.h -*- C++ -*-===//
//
//
//===----------------------------------------------------------------------===//
#ifndef LINER_H
#define LINER_H

#include "llvm/ADT/StringRef.h"
#include "llvm/Support/ErrorOr.h"

namespace LCC {

class Liner {
  llvm::StringRef text;

public:
  Liner(llvm::StringRef t) : text(t) {}
  llvm::ErrorOr<llvm::StringRef> nextLine();
};

} // namespace LCC

#endif /* LINER_H */
