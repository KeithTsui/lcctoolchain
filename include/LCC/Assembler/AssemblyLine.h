//===-- AssemblyLine.h -*- C++ -*-===//
//
//
//===----------------------------------------------------------------------===//
#ifndef UTILITIES_ASSEMBLYLINE_H
#define UTILITIES_ASSEMBLYLINE_H
#include "llvm/ADT/StringRef.h"
#include "llvm/Support/ErrorOr.h"
#include <cassert>
#include <utility>
namespace LCC {

struct AssemblyLine final {
  struct Label {
    llvm::StringRef name;
  };

  struct Directive {
    llvm::StringRef name;
    llvm::StringRef argument;
  };

  struct Instruction {
    llvm::StringRef content;
  };

  struct Comment {
    llvm::StringRef content;
  };

  llvm::StringRef line;
  llvm::Optional<Label> label;
  llvm::Optional<Directive> directive;
  llvm::Optional<Instruction> instruction;
  llvm::Optional<Comment> comment;

  std::string prettyPrint() const;

  bool empty() {
    return !label.hasValue() && !directive.hasValue() &&
           !instruction.hasValue() && !comment.hasValue();
  }

  bool isCommentOnly() {
    return !label.hasValue() && !directive.hasValue() &&
           !instruction.hasValue() && comment.hasValue();
  }

  bool isNoInstructionOrDirective() {
    return !directive.hasValue() && !instruction.hasValue();
  }

  bool isInstruction() { return instruction.hasValue(); }

  bool isDirective() { return directive.hasValue(); }

  /**
     <AsmLine> -> <Label>? (<Directive> | <Instruction> )? <Comment>? '\n'
     <Lable> -> <label-character-string> ':'
     <Directive> -> <Directive-Identifier-String> ' ' <Directive-argument>
     <Instruction> -> <OpCode-String> <Operand-String>*
     <Comment> -> '#'<comment-character-string>
   */
  static llvm::Optional<AssemblyLine> parseAssemblyLine(llvm::StringRef line);
  static std::pair<llvm::Optional<Label>, llvm::StringRef>
  parseLabel(llvm::StringRef line);
  static std::pair<llvm::Optional<Directive>, llvm::StringRef>
  parseDirective(llvm::StringRef line);
  static std::pair<llvm::Optional<Instruction>, llvm::StringRef>
  parseInstruction(llvm::StringRef line);
  static std::pair<llvm::Optional<Comment>, llvm::StringRef>
  parseComment(llvm::StringRef line);
};

} // namespace LCC

#endif /* UTILITIES_ASSEMBLYLINE_H */
