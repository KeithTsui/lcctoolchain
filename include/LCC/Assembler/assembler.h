//===-- assembler.h -*- C++ -*-===//
//
//
//===----------------------------------------------------------------------===//
#ifndef ASSEMBLER_H
#define ASSEMBLER_H

#include "LCC/LCC.h"
#include "llvm/ADT/StringMap.h"
#include "llvm/Support/FileSystem.h"
#include "llvm/Support/MemoryBuffer.h"
#include <_types/_uint8_t.h>
#include <cstddef>
#include <map>
#include <memory>
#include <set>
#include <string>
#include <system_error>
#include <vector>

namespace LCC {

/**
  Assembler, is to assemble assembly file into object file.
  And it is not intended to be inherited.
*/

class Assembler final {

public:
  /**
    This is the symbol table for assembler,
    Which records the symbol and its location in object file.
  */
  using SymbolTable = llvm::StringMap<size_t>;

private:
  std::string inputFilename;
  std::string outputFilename;
  std::unique_ptr<llvm::MemoryBuffer> inputBuffer;
  std::vector<uint8_t> codeBuffer;
  std::vector<uint8_t> headerBuffer;
  std::unique_ptr<llvm::raw_fd_ostream> output;
  std::error_code EC;

  SymbolTable symbolTable;
  llvm::SmallVector<uint16_t, 1> startTable;
  llvm::StringMap<uint16_t> globalTable;
  llvm::StringMap<uint16_t> externalTable;
  std::map<uint16_t, std::string> externalReferenceTable11bit;
  std::map<uint16_t, std::string> externalReferenceTable9bit;
  std::map<uint16_t, std::string> externalReferenceTable16bit;
  std::map<uint16_t, std::string> localReferenceTable;

  /**
   private constructor. which is not used to create an assembler directly.
   use Assembler::make() instead.
   @param the filepath for input assembly file, which is not empty and
   exists.
   @param the filepath for output object file, which is not empty and
   writable.
  */
  Assembler(std::string inputFilename, std::string outputFilename);

public:
  struct Instruction {
    llvm::StringRef oper;
    std::vector<llvm::StringRef> operands;
    static std::unique_ptr<Instruction> make(llvm::StringRef instruction);

  private:
    Instruction() {}

  public:
    void print();
  };

  static std::unique_ptr<Assembler> make(std::string inputFilename,
                                         std::string outputFilename = "");
  static llvm::SmallVector<char> toSmallVector(char const *str);
  static llvm::StringRef toStringRef(llvm::SmallVectorImpl<char> const &str);
  static llvm::StringRef removeComment(llvm::StringRef const &line);
  static bool is_number(const llvm::StringRef s);
  static llvm::StringMap<uint8_t> registerSet;
  bool run();
  void writeHeadByte(uint8_t byte);
  void writeHead(uint16_t word);
  void writeHead(llvm::StringRef str);
  void writeOutByte(uint8_t byte);
  void writeOut(uint16_t word);
  void writeOut(llvm::StringRef str);
  void preserveSpace(size_t bytes);
  void loadSymbolTable(llvm::StringRef assemblies);
  bool binaryGen(llvm::StringRef assemblies);
  bool objectFileGen();

  uint16_t parseInstruction(llvm::StringRef instruction, size_t currentAddress);
  static llvm::StringMap<uint16_t> trapSet;
  uint16_t parseInstrTrap(Instruction *instruction, size_t currentAddress);
  static llvm::StringMap<uint16_t> brSet;
  uint16_t parseInstrBr(Instruction *instruction, size_t currentAddress);
  static llvm::StringMap<uint16_t> addSet;
  uint16_t parseInstrAdd(Instruction *instruction, size_t currentAddress);
  uint16_t parseInstrLd(Instruction *instruction, size_t currentAddress);
  uint16_t parseInstrSt(Instruction *instruction, size_t currentAddress);
  static llvm::StringMap<uint16_t> callSet;
  uint16_t parseInstrCall(Instruction *instruction, size_t currentAddress);
  uint16_t parseInstrAnd(Instruction *instruction, size_t currentAddress);
  uint16_t parseInstrLdr(Instruction *instruction, size_t currentAddress);
  uint16_t parseInstrStr(Instruction *instruction, size_t currentAddress);
  uint16_t parseInstrCmp(Instruction *instruction, size_t currentAddress);
  uint16_t parseInstrNot(Instruction *instruction, size_t currentAddress);
  static llvm::StringMap<uint16_t> memSet;
  uint16_t parseInstrMem(Instruction *instruction, size_t currentAddress);
  uint16_t parseInstrSub(Instruction *instruction, size_t currentAddress);
  uint16_t parseInstrRet(Instruction *instruction, size_t currentAddress);
  uint16_t parseInstrMvi(Instruction *instruction, size_t currentAddress);
  uint16_t parseInstrLea(Instruction *instruction, size_t currentAddress);
};

} // namespace LCC

#endif /* ASSEMBLER_H */
