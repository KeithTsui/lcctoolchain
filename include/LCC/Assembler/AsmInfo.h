//===-- AsmInfo.h -*- C++ -*-===//
//
//
//===----------------------------------------------------------------------===//
#ifndef ASSEMBLER_ASMINFO_H
#define ASSEMBLER_ASMINFO_H

#include "llvm/ADT/SmallVector.h"
#include "llvm/ADT/StringRef.h"

namespace LCC {

struct AsmInfo {
  struct DirectiveInfo {
    llvm::SmallVector<llvm::StringRef, 13> getDirectives() const;
    llvm::StringRef directivePrefix() const;
  };

  DirectiveInfo getDirectiveInfo() const;

};

} // namespace LCC

#endif /* ASSEMBLER_ASMINFO_H */
