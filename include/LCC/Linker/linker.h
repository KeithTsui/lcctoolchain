//===-- linker.h -*- C++ -*-===//
//
//
//===----------------------------------------------------------------------===//
#ifndef LINKER_H
#define LINKER_H

#include "llvm/ADT/SmallVector.h"
#include "llvm/ADT/StringMap.h"
#include "llvm/Support/FileSystem.h"
#include "llvm/Support/MemoryBuffer.h"
#include "llvm/Support/Path.h"
#include <_types/_uint8_t.h>
#include <map>
#include <memory>
#include <string>
#include <vector>

#include "LCC/LCC.h"

namespace LCC {

class Linker {
  llvm::SmallVector<std::unique_ptr<llvm::MemoryBuffer>> inputFiles;
  std::unique_ptr<llvm::raw_fd_ostream> output;
  std::error_code EC;
  llvm::SmallVector<uint16_t, 1> startTable;
  llvm::StringMap<uint16_t> globalTable;
  std::map<uint16_t, std::string> externalReferenceTable11bit;
  std::map<uint16_t, std::string> externalReferenceTable9bit;
  std::map<uint16_t, std::string> externalReferenceTable16bit;
  std::map<uint16_t, std::string> localReferenceTable;

  std::vector<uint16_t> locations;
  std::vector<uint8_t> headerBuffer;
  std::vector<uint8_t> code;

  void writeHeadByte(uint8_t byte);
  void writeHead(uint16_t word);
  void writeHead(llvm::StringRef str);
  bool objectFileGen();

  uint16_t currentLocation() {
    return locations.empty() ? 0 : locations.back();
  }

public:
  // return false if successfully link all input files into output file.
  bool run();
  bool load(std::vector<std::string> inputFilenames,
            std::string outputFilenames = "a.e");

  bool parseEntryS(unsigned char const *&entry);
  bool parseEntryG(unsigned char const *&entry);
  bool parseEntryE(unsigned char const *&entry);
  bool parseEntrye(unsigned char const *&entry);
  bool parseEntryV(unsigned char const *&entry);
  bool parseEntryA(unsigned char const *&entry);
};

} // namespace LCC

#endif /* LINKER_H */
