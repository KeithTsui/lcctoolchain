#include "LCC/Linker/linker.h"
#include "llvm/ADT/SmallVector.h"
#include <vector>

namespace {

LCC::Linker &thisLinker() {
  static LCC::Linker linker;
  return linker;
}

} // namespace

int main(int argc, char *argv[]) {
  if (argc < 2) {
    printf("Wrong number of command line arguments\n");
    printf("Usage: linker <input filename>\n");
    exit(1);
  }

  std::vector<std::string> inputs;
  for (int i = 1; i < argc; ++i) {
    inputs.push_back(argv[i]);
  }
  if (thisLinker().load(inputs)) {
    return 1;
  }
  if (thisLinker().run()) {
    return 1;
  }

  return 0;
}
