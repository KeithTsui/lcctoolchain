//===-- assembler.cpp -*- C++ -*-===//
#include "LCC/Assembler/assembler.h"
#include <cassert>
#include <iostream>

int main(int const argc, char const *const *const argv) {
  if (argc < 2) {
    std::cerr << "wrong argument\n"
              << "usage: asm <input_files>";
    exit(1);
  }
  if (auto assm = LCC::Assembler::make(argv[1])) {
    return assm->run();
  }
  return 1;
}
