# LCCToolchain

This toolchain is for [LCC ISA](https://www.amazon.com/Under-Hood-Anthony-Dos-Reis/dp/1793302898), includes Assemler, Linker, Simulator and Disassembler(Not finished yet).

The LCC ISA is a experimental ISA for educational purpose, therefore it tries to be as simple as possible to support C and C++, as showed in its book, [C and C++ under the hood](https://www.amazon.com/Under-Hood-Anthony-Dos-Reis/dp/1793302898).

## LCC ISA Introduction

LCC ISA is a 16-bit little endian machine with one ALU, 8 general purpose registers(R0~R7), 2 special registers(PC, IR) and status word (nzcv flags). The memory address space is 2^16 in 8-bit Byte, which in its book it is 16-bit Byte, that is word addressable not byte addressable, for my implementation convenience, I modified this specification to 8-bit Byte addressable like common ISA, ARM, X86, RISCV, etc.

Because the registers are 16-bit, particularly PC and IR are 16-bits too, and Memory is 8-bit byte addressable, therefore the natural alignment for native data and binary code is 2 bytes.

Be careful when assemble assembly into binary code.

![LCC Machine](./docs/LCCMachine.png)

## LCC Assembly and Instruction Set
[LCC Instruction Set Summary](/docs/LCCInstructionSetSummary.pdf)


## LCC Execution Mode
![LCC Execution Mode](./docs/LCCExecutionMode.png)


## LCC Executable and Linkable Format (LCC-ELF)
![LCC ELF](./docs/LCCExecutaleAndLinkableFormat.png)

As show in above image, the first byte is `o`, which is the magic byte for LCC Executable and Linkable Format. Then the header entries follow, entries are seperated by `new line` character. Then `C` is the seperator between header and body. The body is consisted of code in binary form, where each instruction is 16 bits, i.e. 2 Bytes.

Linker will load the input files one by one, afterward, check each entry in the headers, to see if needs to do some relocations, then concatenate their bodies.

Simulator is to load the linked LCC-ELF, then execute start from the first instruction indicated by `Start` entry with `S` directive in header plus `load point` and align to `2`.

### Asm Brief Grammar

```bash
# entry for Asm
<Asm> -> <AsmLine-List>
<AsmLine-List> -> <AsmLine>*
<AsmLine> -> <Label>? (<Directive> | <Instruction> )? <Comment>? '\n'
<Lable> -> <label-character-string> ':'
<Directive> -> <Directive-Identifier-String> ' ' <Directive-argument>
<Instruction> -> <OpCode-String> <Operand-String>*
<Comment> -> '#'<comment-character-string>

```

## Build Dependencies

This Toolchain requires LLVM infrastructure to build, therefore you need to install LLVM first bfore build this toolchain.

In Mac OSX, you can use brew to install LLVM directly by using `brew install llvm@12`.

Or if you prefer install LLVM directly from source code, you can visit [Building LLVM with CMake](https://llvm.org/docs/CMake.html). Remeber that this toolchain use LLVM 12 to build, but not test for other LLVM version yet.

## Build Toolchain

```bash
git clone https://gitlab.com/KeithTsui/lcctoolchain.git
cd lcctoolchain
mkdir -p build/Debug
cmake -DCMAKE_BUILD_TYPE=Debug -G Ninja -B ./build/Debug -S . -DCMAKE_INSTALL_PREFIX:PATH=./Install
cmake --build build/Debug
cmake --install build/Debug

```
After those commands executed, the Assembler, Linker and Simulator would be in `lcctoolchain/Install/bin`.

## Demo

You can just run the `test.sh` script to run these two demos. Or follow the instructions to run them one by one manually.

```bash
$ pwd
~/LCCToolchain/test
$ sudo chmod +x test.sh
$ ./test.sh

```

### Demo One for a simple sum of two and five.

```bash
$ cd test
$ ../build/Debug/bin/Asm Sumof2And5.a
$ ../build/Debug/bin/Simulator Sumof2and5.e
What is loaded in memory: 0 -- 16
0a20 0a22 0110 02f0 01f0 00f0 0200 0500
Memory show end.
7
Registers:
R0: 7
R1: 5
R2: 0
R3: 0
R4: 0
R5: 0
R6: 0
R7: 0
IR: 1111000000000000 f000 PC: c
nzcv: 0000
Register show ended.
```

You can see the result of `Sumof2and5.a` is 7 as the output.
Moreover, Simulator will show what is loaded into memory for execution. And just right before the process terminated, it shows the values of all registers.

### Demo Two for Seperated Assembling, Linking and Execution.

Another one more try is generated from [LCCInLLVMLite](https://gitlab.com/KeithTsui/lccinllvmlite), which is the `lcc.a` asm file, generated from following instruction.

```cpp
extern "C" short sum(short a, short b) { return a + b; }

extern "C" short f() {
  short a = 0;
  if (a == 0) {
    a = sum(1, 2);
  }
  return a;
}
```

Use `clang` to generate `LLVM IR` `.ll` file.
```bash
$ ./bin/clang -target msp430-unknown-linux-gnu -S -emit-llvm lcc.cpp
```

Use `llc` to generate `LCC` asm file, `.a`.
```bash
$./bin/llc -march=lcc -filetype=asm -O0 -fast-isel=false -global-isel=false -relocation-model=static lcc.ll  -o lcc.a
```

After we get the `lcc.a` file, for convenience, I have put `lcc.a` in test folder already, let us assemble, linke and execute it.

```bash
$ pwd
~/LCCToolchain/test
```

Use LCC `Asm` to assemble `lcc.a` to `lcc.e`.
```bash
$ ../build/Debug/bin/Asm lcc.a
input: lcc.a
output: lcc.e
```

Use LCC `asm` to assembler `startup.a` to `startup.e`.
```bash
$ ../build/Debug/bin/Asm startup.a
input: startup.a
output: startup.e
```

Use LCC `linker` to link `startup.e` and `lcc.e` to `a.e`.
```bash
$ ../build/Debug/bin/linker startup.e lcc.e
processing file: 1
processing file: 2
file processed: 2
```

Use LCC `Sim` to execute `lcc.e` with `-show-regs-end` option to show the contents of registers just right after execution ends.
```bash
$ ../build/Debug/bin/Sim a.e
What is loaded in memory: 0 -- 76
1648 00f0 be1d 807b 8cab 4261 4463 0110 4cad 806b a21d c0cf ba1d 847b 827f 8cab
00d2 4073 4061 0180 1602 000e bc1d 8ca3 02d0 4270 01d0 4070 ca4f a41d 4071 000e
4061 4cad 826f 846b a61d c0cf
Memory show end.
Registers:
R0: 3
R1: 2
R2: 0
R3: 0
R4: 0
R5: 0
R6: 0
R7: 2
IR: 1111000000000000 f000 PC: 4
nzcv: 0101
Register show ended.
```

The result is sit on the register `R0`, which is used for returning result from a function, if the returning result is fit in a register.

## To-do

1. Add more tests.
2. Use googletest for unit tests for each class and method.
3. Support ELF object file format in Simulator.
4. Use LLD for LCC Linker.
5. Use LLVM LCC backend to generate ELF object file format for LCC.
