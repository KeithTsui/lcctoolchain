sig Utility Program
-------------------

sig changes the one-character file signature of an executable file.
For example, to change the file signature in ttest.e to z, enter

   sig ttest.e z

Then when ttest.e is run with sim, sim will use the microcode in
z.m. Alternatively, you can run the original ttest.e file with 
sim using the z.m microcode by entering

   sim ttest.e -m z


Which File to Use 
-----------------

Windows:   sig.exe or sigwin.exe (identical files)
Mac:       sig or sigmac (identical files)
Linxu:     siglnx
Raspbian:  sigrasp 