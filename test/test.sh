#!/usr/bin/env bash

echo "Demo: Sum of two and five."
../build/Debug/bin/Asm Sumof2And5.a && \
    ../build/Debug/bin/Sim Sumof2and5.e

echo ""
echo "Demo: Seperated Assemble then link and execute."
../build/Debug/bin/Asm lcc.a && \
    ../build/Debug/bin/Asm startup.a && \
    ../build/Debug/bin/linker startup.e lcc.e && \
    ../build/Debug/bin/Sim a.e
