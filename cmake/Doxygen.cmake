find_package(Doxygen REQUIRED)

#include(FetchContent)
FetchContent_Declare(doxygen-awesome-css
  GIT_REPOSITORY
  https://github.com/jothepro/doxygen-awesome-css.git
  GIT_TAG
  v1.6.0
  )
# FetchContent_MakeAvailable(doxygen-awesome-css)
FetchContent_GetProperties(doxygen-awesome-css)
if(NOT doxygen-awesome-css_POPULATED)
  FetchContent_MakeAvailable(doxygen-awesome-css)
  # FetchContent_Populate(doxygen-awesome-css)
  # #create gt build directory in binary tree
  # add_subdirectory(${doxygen-awesome-css_SOURCE_DIR} doxygen)
endif()

# Doxygen(target including_dirs_list)
# e.g. Doxygen(myTarget include lib src)
function(Doxygen target)
  set(NAME "doxygen-${target}")
  set(DOXYGEN_HTML_OUTPUT     ${PROJECT_BINARY_DIR}/${NAME})
  set(DOXYGEN_GENERATE_HTML         YES)
  set(DOXYGEN_GENERATE_TREEVIEW     YES)
  set(DOXYGEN_HAVE_DOT              YES)
  set(DOXYGEN_DOT_IMAGE_FORMAT      svg)
  set(DOXYGEN_DOT_TRANSPARENT       YES)
  set(DOXYGEN_HTML_EXTRA_STYLESHEET
    ${doxygen-awesome-css_SOURCE_DIR}/doxygen-awesome.css)

  set(dirs)
  foreach(dir ${ARGN})
    list(APPEND dirs ${PROJECT_SOURCE_DIR}/${dir})
  endforeach()

  message(STATUS "##### ${dirs}")

  doxygen_add_docs(${NAME}
    ${dirs}
    COMMENT "Generate HTML documentation"
    )
endfunction()
