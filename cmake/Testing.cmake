enable_testing()

# include(FetchContent)
# set(FETCHCONTENT_QUIET off)
# # set the directory for dependent repositories.
# get_filename_component(deps_base "../_deps"
#   REALPATH BASE_DIR "${CMAKE_BINARY_DIR}")
# set(FETCHCONTENT_BASE_DIR ${deps_base})

FetchContent_Declare(
  googletest
  GIT_REPOSITORY https://github.com/google/googletest.git
  GIT_TAG release-1.11.0
  )

# For Windows: Prevent overriding the parent project's
# compiler/linker settings
set(gtest_force_shared_crt ON CACHE BOOL "" FORCE)

option(INSTALL_GMOCK "Install GMock" OFF)
option(INSTALL_GTEST "Install GTest" OFF)

# FetchContent_MakeAvailable(googletest)
FetchContent_GetProperties(googletest)
if(NOT googletest_POPULATED)
  FetchContent_MakeAvailable(googletest)
  # FetchContent_Populate(googletest)
  # #create gt build directory in binary tree
  # add_subdirectory(${googletest_SOURCE_DIR} googletest)
endif()

include(GoogleTest)
include(Coverage)
include(Memcheck)

macro(AddTests target)
  AddCoverage(${target})
  target_link_libraries(${target} PRIVATE gtest_main gmock)
  gtest_discover_tests(${target})
  AddMemcheck(${target})
endmacro()
