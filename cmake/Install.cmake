include(GNUInstallDirs)
# Support Library
install(TARGETS Utilities
  EXPORT LCCLibrary
  ARCHIVE COMPONENT development
  LIBRARY COMPONENT runtime
  PUBLIC_HEADER DESTINATION ${CMAKE_INSTALL_LIBDIR}/lcc/Utilities
  COMPONENT runtime
)


# Assembler library
install(TARGETS Assembler
  EXPORT LCCLibrary
  ARCHIVE COMPONENT development
  LIBRARY COMPONENT runtime
  PUBLIC_HEADER DESTINATION ${CMAKE_INSTALL_LIBDIR}/lcc/Assembler
  COMPONENT runtime
)

# Linker library
install(TARGETS Linker
  EXPORT LCCLibrary
  ARCHIVE COMPONENT development
  LIBRARY COMPONENT runtime
  PUBLIC_HEADER DESTINATION ${CMAKE_INSTALL_LIBDIR}/lcc/Linker
  COMPONENT runtime
)

# Linker library
install(TARGETS Simulator
  EXPORT LCCLibrary
  ARCHIVE COMPONENT development
  LIBRARY COMPONENT runtime
  PUBLIC_HEADER DESTINATION ${CMAKE_INSTALL_LIBDIR}/lcc/Linker
  COMPONENT runtime
)


# Assembler Tool
install(TARGETS Asm
  EXPORT LCCBinary
  RUNTIME COMPONENT runtime
  PUBLIC_HEADER DESTINATION ${CMAKE_INSTALL_BINDIR}/lcc/Asm
  COMPONENT runtime
)

# Linker Tool
install(TARGETS linker
  EXPORT LCCBinary
  RUNTIME COMPONENT runtime
  PUBLIC_HEADER DESTINATION ${CMAKE_INSTALL_BINDIR}/lcc/linker
  COMPONENT runtime
)

# Simulator Tool
install(TARGETS Sim
  EXPORT LCCBinary
  RUNTIME COMPONENT runtime
  PUBLIC_HEADER DESTINATION ${CMAKE_INSTALL_BINDIR}/lcc/Simulator
  COMPONENT runtime
)


if (UNIX)
  install(CODE "execute_process(COMMAND ldconfig)"
    COMPONENT runtime
  )
endif()

install(EXPORT LCCLibrary
  DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/lcc/cmake
  NAMESPACE LCC::
  COMPONENT runtime
)

install(FILES "LCCConfig.cmake"
  DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/lcc/cmake
)

# # CalcConsole runtime
# install(TARGETS calc_console
#   RUNTIME COMPONENT runtime
# )

# CPack configuration
set(CPACK_PACKAGE_VENDOR "Keith Tsui")
set(CPACK_PACKAGE_CONTACT "pixxf@me.com")
set(CPACK_PACKAGE_DESCRIPTION "LCC Toolchain")
include(CPack)
