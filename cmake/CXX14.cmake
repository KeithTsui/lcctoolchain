# https://stackoverflow.com/questions/46746878/is-it-safe-to-link-c17-c14-and-c11-objects
# C++ compilation compatibility.
set(CMAKE_CXX_STANDARD 14 CACHE STRING "C++ standard to conform to")
set(CMAKE_CXX_STANDARD_REQUIRED YES)
set(CMAKE_CXX_EXTENSIONS NO)
