#include(FetchContent)

FetchContent_Declare(
  FTXTUI
  GIT_REPOSITORY https://github.com/ArthurSonzogni/FTXUI.git
  GIT_TAG        v0.11
)
option(FTXUI_ENABLE_INSTALL "" OFF)
option(FTXUI_BUILD_EXAMPLES "" OFF)
option(FTXUI_BUILD_DOCS "" OFF)

#FetchContent_MakeAvailable(FTXTUI)

FetchContent_GetProperties(FTXTUI)
if(NOT FTXTUI_POPULATED)
  FetchContent_MakeAvailable(FTXTUI)
  # FetchContent_Populate(FTXTUI)
  # #create gt build directory in binary tree
  # add_subdirectory(${FTXTUI_SOURCE_DIR} ftxtui)
endif()
