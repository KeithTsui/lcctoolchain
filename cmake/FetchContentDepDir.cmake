include(FetchContent)
set(FETCHCONTENT_QUIET off)
# set the directory for dependent repositories.
get_filename_component(deps_base "../_deps"
  REALPATH BASE_DIR "${CMAKE_BINARY_DIR}")
set(FETCHCONTENT_BASE_DIR ${deps_base})
