//===-- AsmInfo.cpp -*- C++ -*-===//
//
//
//===----------------------------------------------------------------------===//

#include "LCC/Assembler/AsmInfo.h"

namespace {

char drtWord[] = ".word";
char drtFill[] = ".fill";
char drtZero[] = ".zero";
char drtSpace[] = ".space";
char drtBlkw[] = ".blkw";
char drtString[] = ".string";
char drtStringz[] = ".stringz";
char drtAsciz[] = ".asciz";
char drtStart[] = ".start";
char drtGlobal[] = ".global";
char drtGlobl[] = ".globl";
char drtExtern[] = ".extern";
char drtOrg[] = ".org";
char drtPrefix[] = ".";
} // namespace

namespace LCC {

llvm::SmallVector<llvm::StringRef, 13>
AsmInfo::DirectiveInfo::getDirectives() const {
  return {{drtWord},   {drtFill},    {drtZero},  {drtSpace}, {drtBlkw},
          {drtString}, {drtStringz}, {drtAsciz}, {drtStart}, {drtGlobal},
          {drtGlobl},  {drtExtern},  {drtOrg}};
}
llvm::StringRef AsmInfo::DirectiveInfo::directivePrefix() const {
  return {drtPrefix};
}

AsmInfo::DirectiveInfo AsmInfo::getDirectiveInfo() const {
  return DirectiveInfo();
}

} // namespace LCC
