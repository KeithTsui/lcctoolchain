//===-- AssemblyLine.cpp -*- C++ -*-===//
#include "LCC/Assembler/AssemblyLine.h"
#include "LCC/Assembler/AsmInfo.h"
#include "llvm/ADT/SmallVector.h"
#include <cstddef>
#include <sstream>
#include <string>
#include <unordered_set>
#include <utility>

namespace {
std::unordered_set<char> impossibleLabelChar = {' ',  '\t', '\v', '\f',
                                                '\r', '"',  '#'};
std::unordered_set<char> directiveNameEnds = {' ',  '\t', '\v', '\f',
                                              '\r', '\n', '#'};

// std::unordered_set<char> directiveArgumentEnds = {' ',  '\t', '\v', '\f',
//                                                   '\r', '\n', '#'};

std::unordered_set<char> directiveArgumentEnds = {'\n', '#'};

std::unordered_set<char> directiveArgumentImpossibleBegins = {'#'};

std::unordered_set<char> commentBegins = {'#'};
} // namespace

namespace LCC {

std::string AssemblyLine::prettyPrint() const {
  std::stringstream ss;
  ss << line.str() << "\n";
  if (label)
    ss << "label: " << label.getValue().name.str() << "\n";
  if (directive)
    ss << "directive: " << directive.getValue().name.str() << " "
       << directive.getValue().argument.str() << "\n";
  if (instruction)
    ss << "instruction: " << instruction.getValue().content.str() << "\n";
  if (comment)
    ss << "comment: " << comment.getValue().content.str() << "\n";
  return ss.str();
}

llvm::Optional<AssemblyLine>
AssemblyLine::parseAssemblyLine(llvm::StringRef line) {
  if (line.contains_insensitive('\n'))
    return {};
  AssemblyLine ret;
  ret.line = line;
  if (line.empty())
    return ret;

  auto label = parseLabel(line);
  ret.label = label.first;
  auto directive = parseDirective(label.second);
  ret.directive = directive.first;
  auto rest = directive.second;
  if (!directive.first) {
    auto instruction = parseInstruction(directive.second);
    ret.instruction = instruction.first;
    rest = instruction.second;
  }
  auto comment = parseComment(rest);
  ret.comment = comment.first;

  if (comment.second.empty())
    return ret;

  return {};
}

std::pair<llvm::Optional<AssemblyLine::Label>, llvm::StringRef>
AssemblyLine::parseLabel(llvm::StringRef line) {
  llvm::StringRef org = line;

  // label ends with ':'
  auto labelSize = line.find_first_of(':');
  if (labelSize != llvm::StringRef::npos) {
    auto potentialLabel = llvm::StringRef(line.begin(), labelSize);
    bool valid = true;
    for (auto c : impossibleLabelChar)
      valid = valid && !potentialLabel.contains_insensitive(c);
    if (valid)
      return std::make_pair(AssemblyLine::Label{potentialLabel.trim()},
                            line.drop_front(labelSize + 1));
  }

  // if not a label, return original line.
  return {{}, org};
}

std::pair<llvm::Optional<AssemblyLine::Directive>, llvm::StringRef>
AssemblyLine::parseDirective(llvm::StringRef line) {
  llvm::StringRef org = line;
  line = line.ltrim();
  auto asmInfo = AsmInfo();
  auto direcitveInfo = asmInfo.getDirectiveInfo();
  // fix me, if direcitve prefix is more than one character.
  if (!line.empty() && line[0] == direcitveInfo.directivePrefix()[0]) {
    auto directiveNameBegin = line.begin();
    auto directiveNameEnd = line.begin();
    do {
      ++directiveNameEnd;
    } while (directiveNameEnd != line.end() &&
             !directiveNameEnds.count(*directiveNameEnd));
    auto directiveSize = directiveNameEnd - directiveNameBegin;
    auto directiveName = llvm::StringRef(directiveNameBegin, directiveSize);
    line = line.drop_front(directiveSize);
    line = line.ltrim();

    // get directive argument if exists
    if (!line.empty() && !directiveArgumentImpossibleBegins.count(line[0])) {
      auto directiveArgumentBegin = line.begin();
      auto directiveArgumentEnd = line.begin();
      llvm::StringRef argument;
      size_t argumentSize;

      // if (*directiveArgumentBegin != '"') {
      do {
        ++directiveArgumentEnd;
      } while (directiveArgumentEnd != line.end() &&
               !directiveArgumentEnds.count(*directiveArgumentEnd));
      argumentSize = directiveArgumentEnd - directiveArgumentBegin;
      argument = llvm::StringRef(directiveArgumentBegin, argumentSize);
      line = line.drop_front(argumentSize);
      // if (*argument.begin() == '"' && *argument.end() == '"')
      //   argument = argument.drop_front().drop_back();
      // } else {
      //   ++directiveArgumentBegin;
      //   auto stringEnd = line.find_first_of('"', 1);
      //   assert(stringEnd != line.npos && "string quotation not match");
      //   directiveArgumentEnd = directiveArgumentBegin + stringEnd - 1;
      //   argumentSize = directiveArgumentEnd - directiveArgumentBegin;
      //   argument = llvm::StringRef(directiveArgumentBegin, argumentSize);
      //   line = line.drop_front(argumentSize + 2);
      // }

      return {AssemblyLine::Directive{directiveName.trim(), argument.trim()},
              line};
    }

    return {AssemblyLine::Directive{directiveName.trim(), {}}, line};
  }
  return {{}, org};
}

std::pair<llvm::Optional<AssemblyLine::Instruction>, llvm::StringRef>
AssemblyLine::parseInstruction(llvm::StringRef line) {
  llvm::StringRef org = line;
  line = line.ltrim();

  auto asmInfo = AsmInfo();
  auto direcitveInfo = asmInfo.getDirectiveInfo();
  // fix me, if direcitve prefix is more than one character.
  if (!line.empty() && line[0] != direcitveInfo.directivePrefix()[0] &&
      !commentBegins.count(line[0])) {
    auto instBegin = line.begin();
    auto instEnd = line.begin();
    do {
      ++instEnd;
    } while (instEnd != line.end() && !commentBegins.count(*instEnd));

    size_t instSize = instEnd - instBegin;
    line = line.drop_front(instSize);
    auto inst = llvm::StringRef{instBegin, instSize};
    return {AssemblyLine::Instruction{inst.trim()}, line};
  }
  return {{}, org};
}

std::pair<llvm::Optional<AssemblyLine::Comment>, llvm::StringRef>
AssemblyLine::parseComment(llvm::StringRef line) {
  llvm::StringRef org = line;
  line = line.ltrim();
  if (commentBegins.count(*line.begin())) {
    size_t size = line.end() - line.begin();
    line = line.drop_front(size);
    return {AssemblyLine::Comment{llvm::StringRef{line.begin(), size}.trim()},
            line};
  }
  return {{}, org};
}

} // namespace LCC
