#include "LCC/Assembler/assembler.h"
#include "LCC/Assembler/AssemblyLine.h"
#include "LCC/LCC.h"
#include "LCC/Utilities/LCCMachineInfo.h"
#include "LCC/Utilities/liner.h"
#include "LCC/Utilities/utilities.h"
#include "llvm/ADT/SmallVector.h"
#include "llvm/ADT/StringMap.h"
#include "llvm/ADT/Twine.h"
#include "llvm/Support/FileSystem.h"
#include "llvm/Support/MemoryBuffer.h"
#include "llvm/Support/Path.h"
#include <_types/_uint16_t.h>
#include <_types/_uint8_t.h>
#include <algorithm>
#include <cassert>
#include <cctype>
#include <cstddef>
#include <cstdlib>
#include <iostream>
#include <memory>
#include <ostream>
#include <string>
#include <vector>

namespace LCC {

void Assembler::Instruction::print() {
  std::string ops;
  for (auto op : operands) {
    ops += op.str() + ", ";
  }
  llvm::errs() << "Instruction: " << oper.str() << " " << ops << '\n';
}

std::unique_ptr<Assembler> Assembler::make(std::string inputFilename,
                                           std::string outputFilename) {
  if (inputFilename.empty())
    return nullptr;

  auto inputFileExtension = llvm::sys::path::extension(inputFilename);

  if (inputFileExtension.drop_front() != LCC::assemblyExtension) {
    llvm::errs() << "not an assembly file of extension "
                 << inputFileExtension.str() << "\n";
    return nullptr;
  }

  if (!llvm::sys::fs::exists(inputFilename)) {
    llvm::errs() << "not exist " << inputFilename << '\n';
    return nullptr;
  }

  if (outputFilename.empty()) {
    auto oName = toSmallVector(inputFilename.c_str());
    llvm::sys::path::replace_extension(oName, LCC::objectExtension);
    outputFilename = toStringRef(oName).str();
  }

  return std::unique_ptr<Assembler>(
      new Assembler(inputFilename, outputFilename));
}

template <typename T>
std::ostream &operator<<(std::ostream &out,
                         llvm::SmallVectorImpl<T> const &msg) {
  for (auto &element : msg) {
    out << element;
  }
  return out;
}

Assembler::Assembler(std::string inputFilename, std::string outputFilename)
    : inputFilename{inputFilename}, outputFilename{outputFilename} {
  assert((!inputFilename.empty()) && "input file name path cannot be empty.");
  assert((!outputFilename.empty()) && "output file name path cannot be empty.");
}

bool Assembler::run() {
  std::cout << "input: " << inputFilename << '\n'
            << "output: " << outputFilename << '\n';
  auto inputOrError = llvm::MemoryBuffer::getFile(inputFilename);
  if (!inputOrError) {
    llvm::errs() << "cannot open file \n";
    exit(1);
  }

  inputBuffer = std::move(inputOrError.get());

  output.reset(new llvm::raw_fd_ostream(outputFilename, EC));

  auto assemblies = inputBuffer->getBuffer();

  // fisrt pass to generate symbol table
  loadSymbolTable(assemblies);

  //std::cout << "bin gen: " << std::endl;

  // second pass to generate binary into file
  auto result = binaryGen(assemblies);
  if (!result) {
    result = objectFileGen();
  }

  output->flush();
  output->close();
  return result;
}

llvm::SmallVector<char> Assembler::toSmallVector(char const *str) {
  llvm::SmallVector<char> ret;
  while (*str != '\0') {
    ret.push_back(*str++);
  }
  return ret;
}

llvm::StringRef Assembler::toStringRef(llvm::SmallVectorImpl<char> const &str) {
  return llvm::StringRef(str.data(), str.size());
}

llvm::StringRef Assembler::removeComment(llvm::StringRef const &line) {
  auto commentLoc = line.find_first_of(';');
  if (commentLoc != llvm::StringRef::npos) {
    return llvm::StringRef(line.begin(), commentLoc);
  }
  commentLoc = line.find_first_of('#');
  if (commentLoc != llvm::StringRef::npos) {
    return llvm::StringRef(line.begin(), commentLoc);
  }
  return line;
}

// ensure little endian for LCC architecture
void Assembler::writeHeadByte(uint8_t byte) { headerBuffer.push_back(byte); }
void Assembler::writeHead(uint16_t word) {
  auto w = reinterpret_cast<unsigned char *>(&word);
  headerBuffer.push_back(*w++);
  headerBuffer.push_back(*w);
}

void Assembler::writeHead(llvm::StringRef str) {
  for (auto c : str) {
    writeHeadByte(c);
  }
  writeHeadByte('\0'); // string should be ended with \0;
}
void Assembler::writeOutByte(uint8_t byte) { codeBuffer.push_back(byte); }
void Assembler::writeOut(uint16_t word) {
  auto w = reinterpret_cast<unsigned char *>(&word);
  codeBuffer.push_back(*w++);
  codeBuffer.push_back(*w);
}

void Assembler::writeOut(llvm::StringRef str) {
  for (auto c : str) {
    writeOutByte(c);
  }
}

void Assembler::preserveSpace(size_t bytes) {
  while (bytes--) {
    writeOutByte(0);
  }
}

bool Assembler::is_number(const llvm::StringRef s) {
  return !s.empty() && std::find_if(s.begin(), s.end(), [](unsigned char c) {
                         return !std::isdigit(c);
                       }) == s.end();
}

void Assembler::loadSymbolTable(llvm::StringRef assemblies) {
  size_t currentAddress{0};
  size_t lineNumber{0};
  LCC::Liner liner{assemblies};
  symbolTable.clear();
  while (auto line = liner.nextLine()) {
    ++lineNumber;
    auto curLine = line.get().ltrim().rtrim();
    auto asmLineOpt = AssemblyLine::parseAssemblyLine(curLine);
    if (!asmLineOpt) {
      // error
      llvm::errs() << curLine << "\n";
      llvm::errs().flush();
      abort();
    }
    auto asmLine = asmLineOpt.getPointer();
    if (asmLine->empty() || asmLine->isCommentOnly())
      continue;

    auto labelOpt = asmLine->label;
    if (labelOpt) {
      auto label = labelOpt.getPointer();
      symbolTable[label->name] = currentAddress;
    }

    if (asmLine->isNoInstructionOrDirective())
      continue;

    if (asmLine->isDirective()) {
      auto directive = asmLine->directive.getPointer()->name;
      auto argument = asmLine->directive.getPointer()->argument;
      if (directive == ".fill" || directive == ".word") {
        currentAddress += 2;
      } else if (directive == ".blkw" || directive == ".space" ||
                 directive == "zero") {
        if (argument.empty())
          currentAddress += 2;
        else { // it's a number
          long converted = strtol(argument.str().c_str(), NULL, 10) * 2;
          // actually no need to align to 2, because it is a multiple of 2
          // already.
          converted = llvm::alignTo(converted, 2);
          currentAddress += converted;
        }
      } else if (directive == ".stringz" || directive == ".string" ||
                 directive == ".asciz") {
        // remove double quotation and append '\0' at the end
        auto strlen = argument.size() - 2 + 1;
        currentAddress += llvm::alignTo(strlen, 2);
      } else if (directive == ".orig") {
        if (!std::isalpha(*argument.begin())) {
          long converted = strtol(argument.str().c_str(), NULL, 10);
          currentAddress = converted;
        }
      } else {
        //llvm::errs() << "ignore not relevant directive: " << directive << "\n";
        continue;
      }

    } else if (asmLine->isInstruction()) {
      currentAddress += 2;
    } else {
      llvm::errs() << curLine << "\n";
      llvm_unreachable("meet an unknown line.");
    }
  }
}

bool Assembler::binaryGen(llvm::StringRef assemblies) {
  size_t currentAddress{0};
  size_t lineNumber{0};
  LCC::Liner liner{assemblies};
  while (auto line = liner.nextLine()) {
    ++lineNumber;

    ++lineNumber;
    auto curLine = line.get().ltrim().rtrim();
    auto asmLineOpt = AssemblyLine::parseAssemblyLine(curLine);
    if (!asmLineOpt) {
      llvm::errs() << curLine << "\n";
      abort();
    }
    auto asmLine = asmLineOpt.getPointer();
    if (asmLine->isNoInstructionOrDirective())
      continue;

    if (asmLine->isDirective()) {
      auto directive = asmLine->directive.getPointer()->name;
      auto argument = asmLine->directive.getPointer()->argument;

      // xx: .fill 6; xx: .fill label
      if (directive == ".fill" || directive == ".word") {
        if (std::isalpha(argument.front())) { // it's a label
          uint16_t ret = 0;
          if (symbolTable.count(argument)) { // local reference
            ret = static_cast<uint16_t>(symbolTable[argument]);
            localReferenceTable[currentAddress] = argument.str();
          } else { // external reference
            externalReferenceTable16bit[currentAddress] = argument.str();
          }
          writeOut(ret);
        } else { // it's a number
          long converted = strtol(argument.str().c_str(), NULL, 10);
          writeOut(static_cast<uint16_t>(converted));
        }
        currentAddress += 2;
      } else if (directive == ".blkw" || directive == ".space" ||
                 directive == "zero") {
        if (std::isalpha(argument.front())) { // it's a label
          std::cerr << "error on parsing .blkw directive\n";
          return true;
        } else { // it's a number
          long converted = strtol(argument.str().c_str(), NULL, 10) * 2;
          // actually no need to align to 2, because it is a multiple of 2
          // already.
          converted = llvm::alignTo(converted, 2);
          preserveSpace(converted);
          currentAddress += converted;
        }
      } else if (directive == ".stringz" || directive == ".string" ||
                 directive == ".asciz") {
        // remove double quation.
        writeOut(argument.drop_front().drop_front());
        writeOutByte('\0');
        auto strlen = argument.size() + 1; // append '\0' at the end
        // alignment to 2.
        currentAddress += llvm::alignTo(strlen, 2);
      } else if (directive == ".start") {
        startTable.push_back(symbolTable[argument]);
      } else if (directive == ".global" || directive == ".globl") {
        globalTable[argument] = symbolTable[argument];
      } else if (directive == ".extern") {
        externalTable[argument] = 0;
      } else if (directive == ".orig") {
        if (!std::isalpha(argument.front())) { // it's a number
          long converted = strtol(argument.str().c_str(), NULL, 10);
          currentAddress = converted;
        }
      }
    } else if (asmLine->isInstruction()) {
      // it's an instruction
      auto inst = asmLine->instruction.getPointer()->content;
      writeOut(parseInstruction(inst, currentAddress));
      currentAddress += 2;
    } else {
      llvm::errs() << curLine << "\n";
      llvm_unreachable("meet an unknown line when binaryGen().");
      return true;
    }
  }

  return false;
}

bool Assembler::objectFileGen() {

  // output file magic byte
  writeHeadByte(LCC::magicByte);

  // output header
  // start table
  for (auto s : startTable) {
    writeHeadByte('S');
    writeHead(s);
  }

  // global table
  for (auto &p : globalTable) {
    writeHeadByte('G');
    writeHead(p.second);
    writeHead(p.first());
  }
  // externalreferencetable11bit
  for (auto &p : externalReferenceTable11bit) {
    writeHeadByte('E');
    writeHead(p.first);
    writeHead(p.second);
  }
  // externalreferencetable16bit
  for (auto &p : externalReferenceTable16bit) {
    writeHeadByte('V');
    writeHead(p.first);
    writeHead(p.second);
  }
  // externalreferencetable9bit
  for (auto &p : externalReferenceTable9bit) {
    writeHeadByte('e');
    writeHead(p.first);
    writeHead(p.second);
  }
  // localReferencetable
  for (auto &p : localReferenceTable) {
    writeHeadByte('A');
    writeHead(p.first);
    writeHead(p.second);
  }

  // output header-code sperator
  writeHeadByte(LCC::headBodySeperator);

  // output header
  for (auto c : headerBuffer) {
    (*output) << c;
  }

  // output code
  for (auto c : codeBuffer) {
    (*output) << c;
  }

  output->flush();

  return false;
}

std::unique_ptr<Assembler::Instruction>
Assembler::Instruction::make(llvm::StringRef instruction) {
  instruction = instruction.ltrim().rtrim();

  auto splited = instruction.split(' ');
  auto cmd = splited.first;
  if (cmd.empty())
    return nullptr;

  Instruction *instr = new Instruction;
  instr->oper = cmd;

  auto rest = splited.second.ltrim();
  if (!rest.empty()) {
    std::vector<llvm::StringRef> operands;
    while (rest.count(',')) {
      splited = rest.split(',');
      operands.push_back(splited.first);
      rest = splited.second.ltrim();
    }
    operands.push_back(rest);
    instr->operands = std::move(operands);
  }

  return std::unique_ptr<Assembler::Instruction>(instr);
}

uint16_t Assembler::parseInstruction(llvm::StringRef instruction,
                                     size_t currentAddress) {

  auto instr = Instruction::make(instruction);
  auto cmd = instr->oper;
  auto cmdStr = cmd.str();
  std::transform(cmdStr.begin(), cmdStr.end(), cmdStr.begin(), ::tolower);
  auto inst = instr.get();
  if (trapSet.count(cmdStr)) {
    return parseInstrTrap(inst, currentAddress);
  } else if (brSet.count(cmdStr)) {
    return parseInstrBr(inst, currentAddress);
  } else if (cmd == "add") {
    return parseInstrAdd(inst, currentAddress);
  } else if (cmd == "ld") {
    return parseInstrLd(inst, currentAddress);
  } else if (cmd == "st") {
    return parseInstrSt(inst, currentAddress);
  } else if (callSet.count(cmdStr)) {
    return parseInstrCall(inst, currentAddress);
  } else if (cmd == "and") {
    return parseInstrAnd(inst, currentAddress);
  } else if (cmd == "ldr") {
    return parseInstrLdr(inst, currentAddress);
  } else if (cmd == "str") {
    return parseInstrStr(inst, currentAddress);
  } else if (cmd == "cmp") {
    return parseInstrCmp(inst, currentAddress);
  } else if (cmd == "not") {
    return parseInstrNot(inst, currentAddress);
  } else if (memSet.count(cmdStr)) {
    return parseInstrMem(inst, currentAddress);
  } else if (cmd == "sub") {
    return parseInstrSub(inst, currentAddress);
  } else if (cmd == "jmp" || cmd == "ret") {
    return parseInstrRet(inst, currentAddress);
  } else if (cmd == "mvi") {
    return parseInstrMvi(inst, currentAddress);
  } else if (cmd == "lea") {
    return parseInstrLea(inst, currentAddress);
  } else {
    std::string msg = "unregconize asm ";
    msg += instruction;
    llvm_unreachable(msg.c_str());
  }

  return 0;
}

llvm::StringMap<uint8_t> Assembler::registerSet{
    {"r0", R0}, {"r1", R1}, {"r2", R2}, {"r3", R3}, {"r4", R4}, {"r5", R5},
    {"r6", R6}, {"r7", R7}, {"fp", FP}, {"sp", SP}, {"lr", LR}};

static std::string normalizeRegName(llvm::StringRef regName) {
  char first = regName[0];
  if (first == '%' || first == '$')
    regName = regName.drop_front();
  return regName.lower();
}

static uint8_t getRegisterNumByName(llvm::StringRef regName) {
  return Assembler::registerSet[normalizeRegName(regName)];
}

llvm::StringMap<uint16_t> Assembler::trapSet{
    {"halt", 0}, {"nl", 1},   {"dout", 2}, {"udout", 3}, {"hout", 4},
    {"aout", 5}, {"sout", 6}, {"din", 7},  {"hin", 8},   {"ain", 9},
    {"sin", 10}, {"m", 11},   {"r", 12},   {"s", 13},    {"bp", 14}};
static uint8_t getTrapNumByName(llvm::StringRef trapName) {
  return Assembler::trapSet[trapName.lower()];
}

uint16_t Assembler::parseInstrTrap(Instruction *instruction, size_t) {
  auto cmd = instruction->oper;
  uint16_t ret = 15 << 12;
  uint16_t reg{0};
  if (instruction->operands.size() > 0) {
    reg = (getRegisterNumByName(instruction->operands[0])) << 9;
  }
  uint16_t Code = getTrapNumByName(cmd);
  return ret | Code | reg;
}

llvm::StringMap<uint16_t> Assembler::brSet{
    {"brz", 0}, {"bre", 0}, {"brnz", 1}, {"brne", 1},
    {"brn", 2}, {"brp", 3}, {"brlt", 4}, {"brgt", 5},
    {"brc", 6}, {"brb", 6}, {"br", 7},   {"bral", 7}};
static uint8_t getBrNumByName(llvm::StringRef brName) {
  return Assembler::brSet[brName.lower()];
}

uint16_t Assembler::parseInstrBr(Instruction *instruction,
                                 size_t currentAddress) {
  auto cmd = instruction->oper;
  uint16_t ret = 0 << 12;
  uint16_t offset = currentAddress + 2;
  if (instruction->operands.size() > 0) {
    auto op = instruction->operands[0];
    uint16_t addr = 0;
    if (symbolTable.count(op)) { // local reference
      addr = symbolTable[op];
      // localReferenceTable[currentAddress] = op.str();
      auto diff =
          static_cast<int32_t>(addr) - static_cast<int32_t>(currentAddress + 2);
      offset = convertToUInt16<int32_t>(diff, 9);
    } else if (externalTable.count(op)) { // external reference
      externalReferenceTable9bit[currentAddress] = op.str();
      offset = 0;
    } else {
      std::string msg = "symbol not found: ";
      msg += op;
      llvm_unreachable(msg.c_str());
    }
  }
  uint16_t code = getBrNumByName(cmd);

  return ret | (code << 9) | offset;
}

llvm::StringMap<uint16_t> Assembler::addSet{{"add", 1}};
uint16_t Assembler::parseInstrAdd(Instruction *instruction, size_t) {
  // auto cmd = instruction->oper;
  uint16_t ret = 1 << 12;
  assert(instruction->operands.size() == 3 &&
         "not enough arguments for add instruction.");

  auto op0 = instruction->operands[0];
  auto op1 = instruction->operands[1];
  auto op2 = instruction->operands[2];

  auto op0Id = getRegisterNumByName(op0);
  auto op1Id = getRegisterNumByName(op1);

  uint16_t op2value = {0};
  if (registerSet.count(normalizeRegName(op2))) {
    op2value = getRegisterNumByName(op2);
  } else {
    auto x = strtol(op2.str().c_str(), NULL, 10);
    op2value = convertToUInt16(x, 5);
    op2value = op2value | (1 << 5);
  }

  return ret | (op0Id << 9) | (op1Id << 6) | op2value;
}

uint16_t Assembler::parseInstrLd(Instruction *instruction,
                                 size_t currentAddress) {
  // auto cmd = instruction->oper;
  uint16_t ret = 2 << 12;

  assert(instruction->operands.size() == 2 &&
         "not enough arguments for ld instruction.");

  auto op0 = instruction->operands[0];
  auto op1 = instruction->operands[1];

  auto op0Id = getRegisterNumByName(op0);
  uint16_t opvalue = {0};

  uint16_t addr = 0;
  if (symbolTable.count(op1)) { // local reference
    addr = symbolTable[op1];
    // localReferenceTable[currentAddress] = op1.str();
    auto diff =
        static_cast<int32_t>(addr) - static_cast<int32_t>(currentAddress + 2);
    opvalue = convertToUInt16(diff, 9);
  } else if (externalTable.count(op1)) { // external reference
    externalReferenceTable9bit[currentAddress] = op1.str();
  } else {
    std::string msg = "symbol not found: ";
    msg += op1;
    llvm_unreachable(msg.c_str());
  }

  return ret | (op0Id << 9) | opvalue;
}

uint16_t Assembler::parseInstrSt(Instruction *instruction,
                                 size_t currentAddress) {
  //  auto cmd = instruction->oper;
  uint16_t ret = 3 << 12;

  assert(instruction->operands.size() == 2 &&
         "not enough arguments for ld instruction.");

  auto op0 = instruction->operands[0];
  auto op1 = instruction->operands[1];

  auto op0Id = getRegisterNumByName(op0);
  uint16_t opvalue = {0};

  uint16_t addr = 0;
  if (symbolTable.count(op1)) { // local reference
    addr = symbolTable[op1];
    // localReferenceTable[currentAddress] = op1.str();
    auto diff =
        static_cast<int32_t>(addr) - static_cast<int32_t>(currentAddress + 2);
    opvalue = convertToUInt16<int32_t>(diff, 9);
  } else if (externalTable.count(op1)) { // external reference
    externalReferenceTable9bit[currentAddress] = op1.str();
  } else {
    std::string msg = "symbol not found: ";
    msg += op1;
    llvm_unreachable(msg.c_str());
  }

  return ret | (op0Id << 9) | opvalue;
}

llvm::StringMap<uint16_t> Assembler::callSet{{"bl", 4 << 12},
                                             {"call", 4 << 12},
                                             {"jsr", 4 << 12},
                                             {"blr", 4 << 12},
                                             {"jsrr", 4 << 12}};

uint16_t Assembler::parseInstrCall(Instruction *instruction,
                                   size_t currentAddress) {
  auto cmd = instruction->oper;
  uint16_t ret = 4 << 12;
  uint16_t opval = 0;
  uint16_t opval1 = 0;
  if (cmd == "bl" || cmd == "call" || cmd == "jsr") {
    auto op = instruction->operands[0];
    opval = 1 << 11;
    uint16_t addr = 0;
    if (symbolTable.count(op)) { // local reference
      addr = symbolTable[op];
      // localReferenceTable[currentAddress] = op.str();
      auto diff =
          static_cast<int32_t>(addr) - static_cast<int32_t>(currentAddress + 2);
      opval1 = convertToUInt16(diff, 11);
    } else if (externalTable.count(op)) { // external reference
      externalReferenceTable11bit[currentAddress] = op.str();
    } else {
      std::string msg = "symbol not found: ";
      msg += op;
      llvm_unreachable(msg.c_str());
    }
  } else if (cmd == "blr" || cmd == "jsrr") {
    auto op = instruction->operands[0];
    opval = getRegisterNumByName(op) << 6;
    if (instruction->operands.size() == 2) {
      auto op1 = instruction->operands[1];
      auto addr = strtol(op1.str().c_str(), NULL, 10);
      opval1 = convertToUInt16(addr, 6);
    }
  } else {
    llvm_unreachable("there should one or two arguments in Call instruction.");
  }

  return ret | opval | opval1;
}

uint16_t Assembler::parseInstrAnd(Instruction *instruction, size_t) {
  // auto cmd = instruction->oper;
  uint16_t ret = 5 << 12;

  assert(instruction->operands.size() == 3 &&
         "not enough arguments for add instruction.");

  auto op0 = instruction->operands[0];
  auto op1 = instruction->operands[1];
  auto op2 = instruction->operands[2];

  auto op0Id = getRegisterNumByName(op0);
  auto op1Id = getRegisterNumByName(op1);
  uint16_t op2value = {0};
  if (registerSet.count(normalizeRegName(op2))) {
    op2value = getRegisterNumByName(op2);
  } else {
    auto x = strtol(op2.str().c_str(), NULL, 10);
    op2value = convertToUInt16(x, 5);
    op2value = op2value | (1 << 5);
  }

  return ret | (op0Id << 9) | (op1Id << 6) | op2value;
}

uint16_t Assembler::parseInstrLdr(Instruction *instruction, size_t) {
  // auto cmd = instruction->oper;
  uint16_t ret = 6 << 12;
  uint16_t opval = 0;
  uint16_t opval1 = 0;
  uint16_t opval2 = 0;
  if (instruction->operands.size() == 3) {
    auto op = instruction->operands[0];
    auto op1 = instruction->operands[1];
    auto op2 = instruction->operands[2];
    opval = getRegisterNumByName(op) << 9;
    opval1 = getRegisterNumByName(op1) << 6;

    auto x = strtol(op2.str().c_str(), NULL, 10);
    opval2 = convertToUInt16(x, 6);
  } else {
    llvm_unreachable("there should one or two arguments in ldr instruction.");
  }

  return ret | opval | opval1 | opval2;
}

uint16_t Assembler::parseInstrStr(Instruction *instruction, size_t) {
  uint16_t ret = 7 << 12;
  uint16_t opval = 0;
  uint16_t opval1 = 0;
  uint16_t opval2 = 0;
  if (instruction->operands.size() == 3) {
    auto op = instruction->operands[0];
    auto op1 = instruction->operands[1];
    auto op2 = instruction->operands[2];
    opval = getRegisterNumByName(op) << 9;
    opval1 = getRegisterNumByName(op1) << 6;
    auto x = strtol(op2.str().c_str(), NULL, 10);
    opval2 = convertToUInt16(x, 6);
  } else {
    llvm_unreachable("there should one or two arguments in ldr instruction.");
  }

  return ret | opval | opval1 | opval2;
}
// cmp r0, r1
// cmp r0, 100
uint16_t Assembler::parseInstrCmp(Instruction *instruction, size_t) {
  // auto cmd = instruction->oper;
  uint16_t opcode = 8 << 12;

  assert(instruction->operands.size() == 2 &&
         "not enough arguments for cmp instruction.");

  auto op0 = instruction->operands[0];
  auto op2 = instruction->operands[1];

  // sr
  auto op0Id = getRegisterNumByName(op0);

  uint16_t op2value = {0};
  if (registerSet.count(normalizeRegName(op2))) {
    op2value = getRegisterNumByName(op2);
  } else {
    auto x = strtol(op2.str().c_str(), NULL, 10);
    op2value = convertToUInt16(x, 5);
    op2value = op2value | (1 << 5);
  }

  return opcode | (op0Id << 6) | op2value;
}

uint16_t Assembler::parseInstrNot(Instruction *instruction, size_t) {
  // auto cmd = instruction->oper;
  uint16_t ret = 9 << 12;

  assert(instruction->operands.size() == 2 &&
         "not enough arguments for ld instruction.");

  auto op0 = instruction->operands[0];
  auto op1 = instruction->operands[1];

  auto op0Id = getRegisterNumByName(op0);
  auto op1Id = getRegisterNumByName(op1);

  return ret | (op0Id << 9) | (op1Id << 6);
}

llvm::StringMap<uint16_t> Assembler::memSet{
    {"push", 0}, {"pop", 1},  {"srl", 2}, {"sra", 3}, {"sll", 4},
    {"rol", 5},  {"ror", 6},  {"mul", 7}, {"div", 8}, {"rem", 9},
    {"or", 10},  {"xor", 11}, {"mvr", 12}};
uint16_t Assembler::parseInstrMem(Instruction *instruction, size_t) {
  auto cmd = instruction->oper;
  uint16_t ret = 10 << 12;
  uint16_t reg0{0};
  uint16_t reg1{0};
  if (instruction->operands.size() == 1) {
    reg0 = (getRegisterNumByName(instruction->operands[0])) << 9;
  } else if (instruction->operands.size() == 2) {
    reg0 = (getRegisterNumByName(instruction->operands[0])) << 9;
    reg1 = (getRegisterNumByName(instruction->operands[1])) << 6;
  } else {
    llvm_unreachable("number of arguments is wrong.");
  }
  uint16_t code = memSet[cmd];

  return ret | reg0 | reg1 | code;
}

uint16_t Assembler::parseInstrSub(Instruction *instruction, size_t) {
  // auto cmd = instruction->oper;
  uint16_t ret = 11 << 12;

  assert(instruction->operands.size() == 3 &&
         "not enough arguments for add instruction.");

  auto op0 = instruction->operands[0];
  auto op1 = instruction->operands[1];
  auto op2 = instruction->operands[2];

  auto op0Id = getRegisterNumByName(op0);
  auto op1Id = getRegisterNumByName(op1);
  uint16_t op2value = {0};
  if (registerSet.count(normalizeRegName(op2))) {
    op2value = getRegisterNumByName(op2);
  } else {
    auto x = strtol(op2.str().c_str(), NULL, 10);
    op2value = convertToUInt16(x, 5);
    op2value = op2value | (1 << 5);
  }

  return ret | (op0Id << 9) | (op1Id << 6) | op2value;
}

uint16_t Assembler::parseInstrRet(Instruction *instruction, size_t) {
  // auto cmd = instruction->oper;
  uint16_t ret = 12 << 12;

  uint16_t code = 0b111 << 9;
  uint16_t baser = 0b111 << 6;
  uint16_t offset = 0;
  if (instruction->operands.size() == 2) {
    auto op = instruction->operands[0];
    auto op1 = instruction->operands[1];
    baser = getRegisterNumByName(op) << 6;
    auto x = strtol(op1.str().c_str(), NULL, 10);
    offset = convertToUInt16(x, 6);

  } else if (instruction->operands.size() == 1) {
    auto op = instruction->operands[0];
    auto x = strtol(op.str().c_str(), NULL, 10);
    offset = convertToUInt16(x, 6);

  } else if (instruction->operands.size() == 0) {
    // do nothing
  } else {
    llvm_unreachable("there should one or two arguments in Call instruction.");
  }

  return ret | code | baser | offset;
}

uint16_t Assembler::parseInstrMvi(Instruction *instruction, size_t) {
  // auto cmd = instruction->oper;
  uint16_t ret = 13 << 12;

  assert(instruction->operands.size() == 2 &&
         "not enough arguments for ld instruction.");

  auto op0 = instruction->operands[0];
  auto op1 = instruction->operands[1];

  auto op0Id = getRegisterNumByName(op0);
  uint16_t opvalue = {0};
  auto x = strtol(op1.str().c_str(), NULL, 10);
  opvalue = convertToUInt16(x, 9);

  return ret | (op0Id << 9) | opvalue;
}

uint16_t Assembler::parseInstrLea(Instruction *instruction,
                                  size_t currentAddress) {
  // auto cmd = instruction->oper;
  uint16_t ret = 14 << 12;

  assert(instruction->operands.size() == 2 &&
         "not enough arguments for lea instruction.");

  auto op0 = instruction->operands[0];
  auto op1 = instruction->operands[1];

  auto op0Id = getRegisterNumByName(op0);
  uint16_t opvalue = {0};

  uint16_t addr = 0;
  if (symbolTable.count(op1)) { // local reference
    addr = symbolTable[op1];
    // localReferenceTable[currentAddress] = op1.str();
    auto diff =
        static_cast<int32_t>(addr) - static_cast<int32_t>(currentAddress + 2);
    opvalue = convertToUInt16<int32_t>(diff, 9);
  } else if (externalTable.count(op1)) { // external reference
    externalReferenceTable9bit[currentAddress] = op1.str();
  } else {
    std::string msg = "symbol not found: ";
    msg += op1;
    llvm_unreachable(msg.c_str());
  }

  return ret | (op0Id << 9) | opvalue;
}

} // namespace LCC
