//===-- Linker.cpp -*- C++ -*-===//
#include "LCC/Linker/linker.h"
#include "LCC/LCC.h"
#include "LCC/Utilities/utilities.h"
#include <_types/_uint16_t.h>
#include <_types/_uint8_t.h>
#include <cstdint>
#include <ios>
#include <iterator>
#include <ostream>
#include <utility>
#include <vector>

namespace LCC {

// ensure little endian for LCC architecture
void Linker::writeHeadByte(uint8_t byte) { headerBuffer.push_back(byte); }

void Linker::writeHead(uint16_t word) {
  auto w = reinterpret_cast<unsigned char *>(&word);
  headerBuffer.push_back(*w++);
  headerBuffer.push_back(*w);
}

void Linker::writeHead(llvm::StringRef str) {
  for (auto c : str) {
    writeHeadByte(c);
  }
  writeHeadByte('\0'); // string should be ended with \0;
}

bool Linker::objectFileGen() {

  // output file magic byte
  writeHeadByte(LCC::magicByte);

  // output header to header buffer
  // start table
  for (auto s : startTable) {
    writeHeadByte('S');
    writeHead(s);
  }

  // global table
  for (auto &p : globalTable) {
    writeHeadByte('G');
    writeHead(p.second);
    writeHead(p.first());
  }
  // externalreferencetable11bit
  for (auto &p : externalReferenceTable11bit) {
    writeHeadByte('E');
    writeHead(p.first);
    writeHead(p.second);
  }
  // externalreferencetable16bit
  for (auto &p : externalReferenceTable16bit) {
    writeHeadByte('V');
    writeHead(p.first);
    writeHead(p.second);
  }
  // externalreferencetable9bit
  for (auto &p : externalReferenceTable9bit) {
    writeHeadByte('e');
    writeHead(p.first);
    writeHead(p.second);
  }
  // localReferencetable
  for (auto &p : localReferenceTable) {
    writeHeadByte('A');
    writeHead(p.first);
    writeHead(p.second);
  }

  // output header-code sperator
  writeHeadByte(LCC::headBodySeperator);

  // output header
  for (auto c : headerBuffer) {
    (*output) << c;
  }

  // output code
  for (auto c : code) {
    (*output) << c;
  }

  output->flush();

  return false;
}

bool Linker::load(std::vector<std::string> inputFilenames,
                  std::string outputFilename) {
  if (inputFilenames.empty())
    return true;
  for (auto const &inputFilename : inputFilenames) {
    auto inputOrError = llvm::MemoryBuffer::getFile(inputFilename);
    if (!inputOrError)
      return true;
    inputFiles.push_back(std::move(inputOrError.get()));
  }
  if (outputFilename.empty())
    outputFilename = "./a.e";
  output.reset(new llvm::raw_fd_ostream(outputFilename, EC));
  return false;
}

bool Linker::run() {

  // clean buffers
  startTable.clear();
  globalTable.clear();
  externalReferenceTable11bit.clear();
  externalReferenceTable16bit.clear();
  externalReferenceTable9bit.clear();
  locations.clear();
  headerBuffer.clear();
  code.clear();

  // 1 scan input files in turn to extract their header info.
  // 2 concate those code fragment into one array for output
  // and remember their own start locations in that array.
  // 3 adjust refereces.
  int count = 0;
  for (auto &input : inputFiles) {
    locations.push_back(code.size());
    ++count;

    // Header processing
    auto str = input->getBuffer();
    auto bytePtr = str.bytes_begin();
    uint8_t byte = nextByte(bytePtr);

    std::cout << "processing file: " << count << std::endl;

    // magic byte 'o' should be first word of file.
    if (byte != 'o') {
      std::cout << '1' << std::endl;
      return true;
    }

    for (byte = nextByte(bytePtr); byte != 'C'; byte = nextByte(bytePtr)) {
      if (byte == 'S') {
        if (parseEntryS(bytePtr)) {
          std::cout << '2' << std::endl;
          return true;
        }
      } else if (byte == 'G') {
        if (parseEntryG(bytePtr)) {
          std::cout << '3' << std::endl;
          return true;
        }
      } else if (byte == 'E') {
        if (parseEntryE(bytePtr)) {
          std::cout << '4' << std::endl;
          return true;
        }
      } else if (byte == 'e') {
        if (parseEntrye(bytePtr)) {
          std::cout << '5' << std::endl;
          return true;
        }
      } else if (byte == 'V') {
        if (parseEntryV(bytePtr)) {
          std::cout << '6' << std::endl;
          return true;
        }
      } else if (byte == 'A') {
        if (parseEntryA(bytePtr)) {
          std::cout << '7' << std::endl;
          return true;
        }
      } else {
        std::cout << "8 " << std::hex << (uint16_t)byte << std::endl;
        return true;
      }
    }

    // copy code to code buffer;
    while (bytePtr != str.bytes_end()) {
      uint8_t byte = nextByte(bytePtr);
      code.push_back(byte);
    }

    // resolve references
    // E table
    std::map<uint16_t, std::string> temp;
    temp = externalReferenceTable11bit;
    for (auto &kv : temp) {
      auto key = kv.first;
      auto label = kv.second;
      if (globalTable.count(label)) {
        auto ref = globalTable[label];
        auto fragment = getWord(code.data(), key); // code[key];

        // update fragment
        uint16_t pc = key + 2;
        uint16_t offset = signedSum(ref, 16, pc, 16, true);
        uint16_t addr11 = fragment & bits0_10;
        uint16_t refAddr = signedSum(addr11, 10, offset, 15, false);

        fragment = ((fragment >> 11) << 11) | (refAddr & bits0_10);
        // setback fragment
        setWord(code.data(), key, fragment); // code[key] = fragment;
        externalReferenceTable11bit.erase(key);
      }
    }

    // e table
    temp = externalReferenceTable9bit;
    for (auto &kv : temp) {
      auto key = kv.first;
      auto label = kv.second;
      if (globalTable.count(label)) {
        auto ref = globalTable[label];
        auto fragment = getWord(code.data(), key); // code[key];
        // update fragment
        uint16_t pc = key + 2;
        uint16_t offset = signedSum(ref, 16, pc, 16, true);
        uint16_t addr9 = fragment & bits0_8;
        uint16_t refAddr = signedSum(addr9, 8, offset, 15, false);
        fragment = ((fragment >> 9) << 9) | (refAddr & bits0_8);
        // setback fragment
        setWord(code.data(), key, fragment); // code[key] = fragment;
        externalReferenceTable9bit.erase(key);
      }
    }
    // V table
    temp = externalReferenceTable16bit;
    for (auto &kv : temp) {
      auto key = kv.first;
      auto label = kv.second;
      if (globalTable.count(label)) {
        auto ref = globalTable[label];
        auto fragment = getWord(code.data(), key); // code[key];

        // update fragment
        fragment = signedSum(fragment, 15, ref, 16, false);

        // setback fragment
        setWord(code.data(), key, fragment); // code[key] = fragment;
        externalReferenceTable16bit.erase(key);
      }
    }
    // A table

    temp = localReferenceTable;
    for (auto &kv : temp) {
      auto key = kv.first;
      auto fragment = getWord(code.data(), key); // code[key];

      // local reference in relative pc offset should not be modified
      // should relative pc offset local reference not in this local reference
      // table.

      // only local reference in absolute address should be added with module
      // location.
      fragment += currentLocation();

      // setback fragment
      setWord(code.data(), key, fragment); // code[key] = fragment;
      localReferenceTable.erase(key);
    }
  }

  std::cout << "file processed: " << count << std::endl;
  bool res = objectFileGen();
  output->flush();
  output->close();
  return res;
}

bool Linker::parseEntryS(unsigned char const *&entry) {
  auto startAddr = nextWord(entry);
  startTable.push_back(startAddr + currentLocation());
  return false;
}
bool Linker::parseEntryG(unsigned char const *&entry) {
  auto addr = nextWord(entry);
  auto label = getString(entry);
  globalTable[toString2(label)] = addr + currentLocation();
  return false;
}
bool Linker::parseEntryE(unsigned char const *&entry) {
  auto addr = nextWord(entry);
  auto label = getString(entry);
  externalReferenceTable11bit[addr + currentLocation()] = toString2(label);
  return false;
}
bool Linker::parseEntrye(unsigned char const *&entry) {
  auto addr = nextWord(entry);
  auto label = getString(entry);
  externalReferenceTable9bit[addr + currentLocation()] = toString2(label);
  return false;
}
bool Linker::parseEntryV(unsigned char const *&entry) {
  auto addr = nextWord(entry);
  auto label = getString(entry);
  externalReferenceTable16bit[addr + currentLocation()] = toString2(label);
  return false;
}
bool Linker::parseEntryA(unsigned char const *&entry) {
  auto addr = nextWord(entry);
  auto label = getString(entry);
  localReferenceTable[addr + currentLocation()] = toString2(label);
  return false;
}

} // namespace LCC
