add_library(Simulator Machine.cpp)
llvm_map_components_to_libnames(llvm_libs support)
message (STATUS "Support libs: ${llvm_libs}")
target_link_libraries(Simulator ${llvm_libs} Utilities)
