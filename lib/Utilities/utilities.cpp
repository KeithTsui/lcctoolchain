//===-- utilities.cpp -*- C++ -*-===//

#include "LCC/Utilities/utilities.h"
#include <_types/_uint16_t.h>
#include <_types/_uint8_t.h>
#include <cassert>
#include <cstdlib>
#include <iostream>
#include <limits>
#include <sys/_types/_int16_t.h>
#include <vector>

namespace LCC {

uint16_t nextWord(const unsigned char *&ptr) {
  uint16_t first = *ptr++;
  uint16_t second = *ptr++;
  return first | (second << 8);
}

uint16_t getWord(const unsigned char *ptr, size_t offset) {
  uint16_t lo = ptr[offset];
  uint16_t hi = ptr[offset + 1];
  return lo | (hi << 8);
}

void setWord(unsigned char *ptr, size_t offset, uint16_t word) {
  uint8_t lo = word & 0xff;
  uint8_t hi = (word & 0xff00) >> 8;
  ptr[offset] = lo;
  ptr[offset + 1] = hi;
}

uint8_t nextByte(const unsigned char *&ptr) {
  uint8_t byte = *ptr++;
  return byte;
}

uint16_t toWord(char c) {
  auto x = *reinterpret_cast<uint8_t *>(&c);
  return x;
};
std::vector<uint8_t> getString(const unsigned char *&ptr) {
  std::vector<uint8_t> ret;
  for (uint8_t byte = nextByte(ptr); byte != '\0'; byte = nextByte(ptr)) {
    ret.push_back(byte);
  }
  return ret;
}
std::vector<uint16_t> getWString(const unsigned char *&ptr) {
  std::vector<uint16_t> ret;
  for (uint16_t word = nextWord(ptr); word != toWord('\0');
       word = nextWord(ptr)) {
    ret.push_back(word);
  }
  return ret;
}
std::string toString2(std::vector<uint8_t> const &wstr) {
  std::string x;
  for (auto &w : wstr) {
    x += w;
  }
  return x;
}
std::string toString(std::vector<uint16_t> const &wstr) {
  std::string x;
  for (auto &w : wstr) {
    x += w;
  }
  return x;
}

unsigned char highChar(uint16_t word) {
  return (word & (0b11111111 << 8)) >> 8;
}

unsigned char lowChar(uint16_t word) { return (word & (0b11111111)); }

bool signOf(uint16_t a, size_t pos) { return (a & (0b1 << pos)) >> pos; }
uint16_t negate(uint16_t a) { return ~a + 1; }
uint16_t signComplement(uint16_t, size_t pos) {
  uint16_t complement = 0;
  if (pos < (16 - 1)) {
    complement = 0b1;
    for (int i = pos; i <= (16 - 2); ++i) {
      complement <<= 1;
      complement |= 0b1;
    }
    complement <<= (pos + 1);
  }
  return complement;
}

uint16_t signedExtension(uint16_t a, size_t p) {
  if (p == 16)
    return a;
  return signOf(a, p) ? a | signComplement(a, p) : a;
}

// p, q specify the sign bit position, or the size of a, b
// which are less than or equal to 16.
// e.g pc += offset9, sum(pc, 16, offset9, 8)
uint16_t signedSum(uint16_t a, size_t p, uint16_t b, size_t q,
                   bool isSubtraction) {
  assert(p < 17 && q < 17 && "size should be less than 17");
  auto signExtendedA = signedExtension(a, p);
  auto signExtendedB = signedExtension(b, q);
  if (isSubtraction)
    signExtendedB = negate(signExtendedB);
  uint16_t c = signExtendedA + signExtendedB;
  return c;
}

int16_t Mult(int16_t a, uint16_t b) {
  int16_t product = 0;
  while (b) {
    if (b & 1)
      product += a;
    b >>= 1;
    a <<= 1;
  }
  return product;
}

uint16_t signedMul(uint16_t a, size_t p, uint16_t b, size_t q) {
  assert(p < 17 && q < 17);
  auto signExtendedA = signedExtension(a, p);
  auto signExtendedB = signedExtension(b, q);
  auto signB = signOf(b, q);
  auto positiveB = signB ? negate(signExtendedB) : signExtendedB;
  int16_t A = *reinterpret_cast<int16_t *>(&signExtendedA);
  auto product = Mult(A, positiveB);
  // uint16_t res = *reinterpret_cast<uint16_t *>(&product);
  return signB ? negate(product) : product;
}

int16_t Divi(int16_t a, int16_t b) {
  int quotient = 0;
  while (true) {
    a -= b;
    if (a < 0)
      break;
    ++quotient;
  }
  return quotient;
}

uint16_t signedDiv(uint16_t a, size_t p, uint16_t b, size_t q) {
  // auto signA = signOf(a, p);
  // auto signB = signOf(b, q);
  auto signExtA = signedExtension(a, p);
  auto signExtB = signedExtension(b, q);
  auto signedA = *reinterpret_cast<int16_t *>(&signExtA);
  auto signedB = *reinterpret_cast<int16_t *>(&signExtB);
  auto div = Divi(signedA, signedB);
  return *reinterpret_cast<uint16_t *>(div);
}

void signedSumInplace(uint16_t &a, size_t p, uint16_t b, size_t q,
                      bool isSubtraction) {
  a = signedSum(a, p, b, q, isSubtraction);
}

// ensure little endian for LCC architecture
std::array<uint8_t, 2> toUInt8s(uint16_t word) {
  std::array<uint8_t, 2> ret;
  auto w = reinterpret_cast<unsigned char *>(&word);
  ret[0] = (*w++);
  ret[1] = (*w);
  return ret;
}
std::vector<uint8_t> toUInt8s(llvm::StringRef str) {
  std::vector<uint8_t> ret;
  for (auto c : str) {
    uint16_t word = static_cast<uint16_t>(c);
    auto bytes = toUInt8s(word);
    ret.push_back(bytes[0]);
    ret.push_back(bytes[1]);
  }
  auto bytes = toUInt8s('\0');
  ret.push_back(bytes[0]);
  ret.push_back(bytes[1]);
  return ret;
}

// imm5
int16_t convertBacktoInt(uint16_t a, size_t bits) {
  assert(bits <= 16 && "bits must less than or equal to 16 for uint16_t.");
  uint16_t one = 1;
  uint16_t signBit = one << (bits - 1);
  uint16_t sign = a & signBit;
  uint16_t ret = a;
  if (sign) { // negative, do sign extension
    uint16_t hi = std::pow(2, 16 - bits) - 1;
    hi = hi << bits;
    ret = hi | a;
  }
  return *reinterpret_cast<int16_t *>(&ret);
}

} // namespace LCC
