//===-- Liner.cpp -*- C++ -*-===//
//
//
//===----------------------------------------------------------------------===//

#include "LCC/Utilities/liner.h"
#include <system_error>

namespace LCC {

llvm::ErrorOr<llvm::StringRef> Liner::nextLine() {
  // ensure that text is no empty.
  if (text.empty())
    return std::error_code();

  // where is the first line.
  auto nlPos = text.find('\n');
  auto found = nlPos == llvm::StringRef::npos;

  // return first line if found or whole text if not found a line.
  auto res = found ? text : llvm::StringRef(text.begin(), nlPos);

  // drop the first line if found, or set to empty if not found.
  text = found ? llvm::StringRef() : text.drop_front(nlPos + 1);
  return res;
}

} // namespace LCC
